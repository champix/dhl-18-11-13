'use strict';
jsLoaded();

function jsLoaded() {

    var jsloaded = document.querySelector('#is-js-loading');
    jsloaded.style.display = 'none';
    console.log(arguments);
    console.log('c\'est la balise que tu viens d\'enelever', jsloaded);
}

function checkValidityOfForm() {
    var valid = true;
    //debugger;
    //var returnObject={};
    var titreOfNote = document.forms["form-edit-note"]['input-titre'];
    if (titreOfNote.value.length <= 2) {
        valid = false;
        titreOfNote.classList.add('invalid-input');
    } else {
        titreOfNote.classList.remove('invalid-input');
        titreOfNote = titreOfNote.value;
    };
    //  else returnObject.titre=titreOfNote;

    var date = document.forms["form-edit-note"]['input-date'].value;
    if (undefined == date || (String(date).length != 10 || Date.parse(date) == undefined)) valid = false;

    var time = document.forms["form-edit-note"]['input-time'].value;
    const regex = /^([01]\d|2[0-3]):[0-5]\d$/;
    if (regex.exec(time) == null) valid = false;

    var description = document.forms["form-edit-note"]['input-description'].value;
    if (description == "") valid = false;

    var user = document.forms["form-edit-note"]['input-user'];
    var userName = '';
    if (-1 == user.selectedIndex || user.value == '') valid = false;
    else {
        userName = user.options[user.selectedIndex].innerHTML;
        user = user.value;
    }


    console.log(titreOfNote, date, time, user, description);
    return (valid) ? {
        titre: titreOfNote,
        date: date,
        time: time,
        description: description,
        user: user,
        username: userName
    } : null;
}


function addNoteToList(objNoteToAdd) {
    if (undefined === objNoteToAdd &&
        ((objNoteToAdd = checkValidityOfForm()) == null)) return false;
    console.log(objNoteToAdd);

    var maDivNote = document.createElement('div');
    maDivNote.classList.add('note');
    maDivNote.innerHTML = '<div class="img-close"><img src="img/close.png" /></div><div class="note-user"><img src="img/usr/' + objNoteToAdd.user + '"><br/>' + objNoteToAdd.username + '</div><div class="note-right"><div class="note-titre">' + objNoteToAdd.titre + '</div><div class="note-datetime">' + objNoteToAdd.date + ' à ' + objNoteToAdd.time + '</div></div><div class="note-description">' + objNoteToAdd.description + '</div>';

    document.querySelector('#notes-container').appendChild(maDivNote);
    return true;
}